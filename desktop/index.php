<!DOCTYPE html>
<html lang="id">
<head>
    <title>IndoParents.com | Situs parenting, health dan gaya hidup paling lengkap. Disajikan dengan bahasa ringan dan edukasi.</title>
    <link rel="Shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="Situs parenting, health dan gaya hidup paling lengkap. Disajikan dengan bahasa ringan dan edukasi.">
    <meta property="og:title" content="IndoParents.com" />
    <meta property="og:description" content="Situs parenting, health dan gaya hidup paling lengkap. Disajikan dengan bahasa ringan dan edukasi." />
    <meta property="og:url" content="http://www.indoparents.com" />
    <meta property="og:image" content="http://indoparents.com/img/share.jpg" />
    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="assets/plugins/sticky-sidebar.min.js"></script>
    <link rel="stylesheet" href="assets/plugins/splide-2.4.21/dist/css/splide.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css?<?= time() ?>" />
    <script src="assets/plugins/splide-2.4.21/dist/js/splide.min.js"></script>
    <script src="assets/js/main.js?<?= time() ?>"></script>
</head>
<body>
    <div class="wrapper">
<?php include ("components/header.php"); ?>
        
        <?php
            if(isset($_GET['page'])){ 
                $url = 'index.php?';
                include("pages/".$_GET['page'].".php");
                // include("about/".$_GET['page'].".php");
            }else{
                $url = 'index.php?';
                include("pages/home.php");
            }
        ?>
        <!-- Floating Ads -->
        <div class="floating-ads-left">
            <img src="assets/images/ads2.png" width="160" height="600" alt="ads"/>
        </div>
        <div class="floating-ads-right">
            <img src="assets/images/ads2.png" width="160" height="600" alt="ads"/>
        </div>
<?php include ("components/footer.php"); ?>
    </div>
</body>
</html>