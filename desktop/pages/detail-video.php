<div class="main-content">
    <!-- <?php  // include ("components/artist.php"); ?> -->
    <!-- <div class="ads">
        <a href="?page=detail"><img alt="image" width="970" height="250" src="assets/images/ads1.png"/></a>
    </div> -->
    <div class="content-home" id="content">
        <div class="content-article mt-15">
            <article class="article-detail">
                <h3 class="card-headline-no-image-title-detail"><a href="index.php?page=kanal">VIDEO</a></h3>
                <h1 class="article-detail--title">8 Potret Rommy Sulastyo di Luar Sinetron: Sosok Penyayang Keluarga</h1>
                <p class="article-detail--desc">Kemunculan Rommy Sulastyo sebagai ayah Aldebaran di sinetron Ikatan Cinta mampu menarik perhatian publik.</p>
                <div class="article-detail--info">
                    <div class="author">
                        Admin
                    </div>
                    <div class="date">
                        Kamis, 16 September 2021 | 18:06 WIB 
                    </div>
                </div>

                <div class="share-baru-header">
                    <a href="#">
                        <img src="assets/images/share/fb.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/twitter.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/line.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/tele.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/wa.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/link.svg" alt="">
                    </a>
                </div>

                <div class="article-detail-video">
                    <iframe class="dailymotion-component" width="680" height="365" src="https://www.dailymotion.com/embed/video/x84cm6g?autoplay=1" frameborder="0" allowfullscreen="true"></iframe>
                    <div class="article-detail-video--caption">Nadiem Makarim (Instagram/Kemdikbud.RI)</div>
                </div>
                <div class="article-detail--body">
                    <p><strong>indoparents.com</strong> - Diplomat senior Indonesia Dino Patti Djalal membeberkan tiga bukti yang menunjukkan bahwa seseorang bernama Fredy Kusnadi diduga terlibat dalam kasus penggelapan sertifikat tanah milik ibundanya.</p>
                    <p>Dino menuding Fredy merupakan sindikat mafia tanah yang menggelapkan sertifikat tanah ibundanya tersebut.</p>
                    <p>"Saya ingin memberikan tiga bukti mengenai keterlibatan Fredy dalam sindikat mafia tanah," ujar Dino dalam video yang diunggah di akun Instagram miliknya, Senin (15/2/2021) dini hari.</p>
                    <p>Dino menuturkan, bukti pertama yang dimilikinya, yakni rekaman pengakuan dari seseorang bernama Sherly. Sherly, kata Dino, saat ini telah ditangkap polisi dan berstatus tersangka.</p>
                    <div class="read-also-wrap">
                        <span class="read-also-wrap-title">Baca Juga:</span>
                        <a href="" class="read-also-wrap-item">Hurricane Ida updates: At least 1 death in Louisiana as New Orleans loses power</a>
                    </div>
                    <p>"Saya memberikan apresiasi dan terima kasih karena Sherly telah memberikan pengakuan yang sejujur-jujurnya mengenai peran Fredy dalam salah satu aksi penipuan terhadap rumah ibu saya," ucap Dino sebagaimana dilansir dari Antara.</p>
                    <p>Bukti kedua yang disampaikan Dino, yakni bukti transfer uang. Uang tersebut diduga merupakan bagian dari hasil penggadaian sertifikat rumah milik ibunya di suatu koperasi.</p>
                    <p>"Bukti kedua yang saya miliki dan sudah saya berikan ke polisi adalah bukti transfer yang diterima Fredy sebesar Rp 320 juta. Ini adalah sebagai bagian dari hasil penggadaian sertifikat rumah milik ibu saya ke suatu koperasi. Dari sana diuangkan sekitar Rp 4 (miliar) sampai Rp 5 miliar dan dibagi-bagi di antara mereka. Paling besar mungkin itu bosnya mendapat Rp 1,7 miliar. Yang lain antara Rp 1 miliar dan Rp 500 juta," kata Dino.</p>
                    <div class="read-also-wrap">
                        <span class="read-also-wrap-title">Baca Juga:</span>
                        <a href="" class="read-also-wrap-item">Hurricane Ida updates: At least 1 death in Louisiana as New Orleans loses power</a>
                    </div>
                    <p>"Jadi jelas nama Fredy ada di berbagai kasus rumah, sedikitnya tiga rumah, tapi mungkin lebih dari itu," sambung dia</p>
                </div>
                <div class="article-detail-pagination">
                    <a href="" class="active">1</a>
                    <a href="?page=detail">2</a>
                    <a href="?page=detail">3</a>
                    <a href="?page=detail">4</a>
                    <a href="" class="show-all">Tampilkan Semua</a>
                </div>
                <div class="article-detail-tag">
                    <span class="label card-headline-no-image-title-detail">Tag</span>
                    <a href="" class="tag-item">SIKM</a>
                    <a href="" class="tag-item">Anies Baswedan</a>
                    <a href="" class="tag-item">PSBB</a>
                </div>
                <!-- <div class="article-detail-share-link">
                    <span>Share link :</span>
                    <a href="" class="share-link-item facebook">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                            <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                        </svg>
                    </a>
                    <a href="" class="share-link-item twitter">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-twitter" viewBox="0 0 16 16">
                            <path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"/>
                        </svg>
                    </a>
                    <a href="" class="share-link-item whatsapp">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-whatsapp" viewBox="0 0 16 16">
                            <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/>
                        </svg>
                    </a>
                    <a href="" class="share-link-item link">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-link-45deg" viewBox="0 0 16 16">
                            <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"/>
                            <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"/>
                        </svg>
                    </a>
                </div> -->
                <div class="share-baru-bottom">
                <span>Share link :</span>
                    <a href="#">
                        <img src="assets/images/share/fb.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/twitter.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/line.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/tele.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/wa.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="assets/images/share/link.svg" alt="">
                    </a>
                </div>
            </article>
            <div class="hot-videos article-detail mt-10">
                <h2 class="hot-videos-title">Hot Video</h2>
                <div>
                    <div class="splide" id="slider-videos-detail">
                        <div class="splide__track">
                            <ul class="splide__list">
                            <li class="splide__slide">
                            <div class="bg-card-video-item-1">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/1.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Daily Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/2.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-1"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-1">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-2">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/3.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Daily Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/4.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-2"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-2">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-1">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/1.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Weekly Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/2.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-1"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-1">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-2">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/3.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Month Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/4.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-2"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-2">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-1">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/1.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Daily Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/2.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-1"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-1">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-2">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/3.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Daily Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/4.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-2"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-2">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-1">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/1.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Weekly Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/2.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-1"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-1">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="bg-card-video-item-2">
                                <article class="card-video-item">
                                    <img alt="image" class="card-video-item--img" width="180" height="135" src="assets/images/thumb7.jpg"/>
                                        <div class="bottom-left-play">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/play.svg"/></a>
                                        </div>    
                                        <div class="bottom-left-bg-detail">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/3.svg"/></a>
                                        </div>
                                        <div class="bottom-left-text">
                                            Month Video 
                                        </div>
                                        <div class="top-right-bg">
                                            <a href="index.php?page=detail"><img alt="image"src="assets/images/hot-video/4.svg"/></a>
                                        </div>
                                    <h4 class="card-video-item--title-hot-video-2"><a href="index.php?page=detail">Innalillahi, 10 Nakes Kabupaten Bogor Meninggal Akibat Covid-19</a></h4>
                                    <div class="card-video-item--time-2">11.11 WIB</div>
                                </article>
                            </div>
                        </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-20">
                <h3 class="card-headline-no-image-title">Parenting</h3>
                <article class="card-one-headline">
                    <img alt="image" class="card-one-headline-img" width="310" height="230" src="assets/images/thumb2.jpg"/>
                    <div class="card-one-headline--info">
                        <h2 class="card-one-headline--title">
                            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                        </h2>
                        <div class="category-and-time">
                            <span>11:15 WIB</span>
                        </div>
                    </div>
                </article>
                <div class="card-two-wrap">
                    <article class="card-two-headline">
                        <div class="card-two-headline-img-wrap">
                            <img alt="image" class="card-two-headline-img" width="100" height="74" src="assets/images/thumb3.jpg"/>
                        </div>
                        <div class="card-two-headline--info">
                            <h4 class="card-two-headline--title">
                                <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                            </h4>
                            <div class="category-and-time">
                                <span>11:15 WIB</span>
                            </div>
                        </div>
                    </article>
                    <article class="card-two-headline">
                        <div class="card-two-headline-img-wrap">
                            <img alt="image" class="card-two-headline-img" width="100" height="74" src="assets/images/thumb3.jpg"/>
                        </div>
                        <div class="card-two-headline--info">
                            <h4 class="card-two-headline--title">
                                <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                            </h4>
                            <div class="category-and-time">
                                <span>11:15 WIB</span>
                            </div>
                        </div>
                    </article>
                    <article class="card-two-headline">
                        <div class="card-two-headline-img-wrap">
                            <img alt="image" class="card-two-headline-img" width="100" height="74" src="assets/images/thumb3.jpg"/>
                        </div>
                        <div class="card-two-headline--info">
                            <h4 class="card-two-headline--title">
                                <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                            </h4>
                            <div class="category-and-time">
                                <span>11:15 WIB</span>
                            </div>
                        </div>
                    </article>
                    <article class="card-two-headline">
                        <div class="card-two-headline-img-wrap">
                            <img alt="image" class="card-two-headline-img" width="100" height="74" src="assets/images/thumb3.jpg"/>
                        </div>
                        <div class="card-two-headline--info">
                            <h4 class="card-two-headline--title">
                                <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                            </h4>
                            <div class="category-and-time">
                                <span>11:15 WIB</span>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="mt-10">
            	<h3 class="card-headline-no-image-title">Terkini</h3>
            	<div class="list">
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Health</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                   <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Konsultasi</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Health</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                   <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Konsultasi</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Health</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                   <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Konsultasi</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="list-element">
                            <article class="main-card">
                                <div class="main-card-img-wrap">
                                    <img alt="image" class="main-card-img" width="350" height="261" src="assets/images/thumb1.jpg"/>
                                </div>
                                <div class="main-card--info">
                                    <h4 class="main-card--title">
                                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                                    </h4>
                                    <p class="main-card--desc">Anak Nella Kharisma dan Dory Harsa telah lahir pada 21 Agustus lalu, secantik apa? </p>
                                    <div class="category-and-time">
                                        <a href="?page=detail">Parenting</a>
                                        <span>11:15 WIB</span>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                <div>
                    <button class="main-card-loadmore" id="loadmore">Tampilkan lebih banyak</button>
                </div>
            </div>
        </div>
        <?php include ("components/sidebar.php"); ?>
    </div>
</div>

<script>
    const loadmore = document.querySelector('#loadmore');
    let currentItems = 5;
    loadmore.addEventListener('click', (e) => {
        const elementList = [...document.querySelectorAll('.list .list-element')];
        for (let i = currentItems; i < currentItems + 5; i++) {
            if (elementList[i]) {
                elementList[i].style.display = 'block';
            }
        }
        currentItems += 5;

        // Load more button will be hidden after list fully loaded
        if (currentItems >= elementList.length) {
            event.target.style.display = 'none';
        }
    })
</script>