<div class="main-content">
    <!-- <?php  // include ("components/artist.php"); ?> -->
    <!-- <div class="ads">
        <a href="?page=detail"><img alt="image" width="970" height="250" src="assets/images/ads1.png"/></a>
    </div> -->
    <div class="content-home" id="content">
        <div class="content-article mt-15">
            <article class="article-detail">
                <h3 class="card-headline-no-image-title-detail">TENTANG KAMI</h3>
                <div class="article-detail--body">

                    <p><strong>Indoparents.com</strong> merupakan situs parenting, kesehatan, dan gaya hidup yang tercipta pada Oktober 2022. Kami hadir untuk memenuhi kebutuhan para orangtua muda dengan mobilitas tinggi. </p>

                    <p>Dikemas dengan kompisisi multimedia serta bahasa yang lentur, Indoparents menjadi website berita kesehatan, gaya hidup, dan parenting yang ideal. Sekaligus menjadi solusi jitu dalam menjaga keharmonisan rumah tangga.</p>

                    <p>Diangkat dari perspektif orangtua muda dan kaum millennials, kami mengemas konten melalui artikel, foto, infografis, dan video.</p>
                    <p>Goals Indoparents sudah barang tentu mengedukasi dan membantu para orangtua muda menciptakan rumah tangga harmonis, penuh kasih sayang, sejahtera, dan sehat. </p>
                    <p>Nikmati sensasi berjelajah di <strong>Indoparents.com</strong>.</p>

                </div>
            </article>

        </div>
        <?php include ("components/sidebar-about.php"); ?>
    </div>
</div>