<div class="main-content">
    <!-- <?php  // include ("components/artist.php"); ?> -->
    <!-- <div class="ads">
        <a href="?page=detail"><img alt="image" width="970" height="250" src="assets/images/ads1.png"/></a>
    </div> -->
    <div class="content-home" id="content">
        <div class="content-article mt-15">
            <article class="article-detail">
                <h3 class="card-headline-no-image-title-detail">REDAKSI</h3>
                <div class="article-detail--body">
                	<div class="list-article">
                    <ul>
                        <li><a href="?page=kanal">Pimpinan Redaksi</a></li>
                            <div class="list-nama">
                                <p>William Bradley Pitt</p>
                            </div>
                        <li><a href="?page=kanal">Redaktur Pelaksana</a></li>
                        	<div class="list-nama">
                                <p>Shiloh Jolie-Pitt</p>
                                <p>Hal Knox Hillhouse</p>
                                <p>Elizabeth Brown</p>
                            </div>
                        <li><a href="?page=kanal">Editor</a></li>
                        	<div class="list-nama">
                                <p>William Alvin Pitt</p>
                                <p>Hal Knox Hillhouse</p>
                                <p>Clara Mae Bell</p>
                            </div>
                    </ul>
                </div>
                </div>
            </article>

        </div>
        <?php include ("components/sidebar-about.php"); ?>
    </div>
</div>