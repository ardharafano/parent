<div class="main-content mt-20 mb-20">
    <!-- <div id="div-ad-top" data-ad-type="msite_top" class="ads ads--top_home">
        <script type="text/javascript">
            googletag.cmd.push(function() { googletag.display('div-ad-top'); });
        </script>
    </div> -->
    <?php // include ("components/artist.php"); ?>

    <a href="#!" rel="">
        <div class="banner-ads--big">
            <img src="assets/images/ads_baru/lead.svg" alt="" width="320px" height="100px">
        </div>
    </a>

    <div class="kanal-wrap">
        <h3 class="base-title-desc">Parenting</h3>
        <div class="date">
            Kamis, 16 September 2021
        </div>
    </div>
    <article class="article-detail">
        <div class="t5-b20">
            <h1 class="article-detail--title">8 Potret Rommy Sulastyo di Luar Sinetron: Sosok Penyayang Keluarga</h1>
            <p class="article-detail--desc">Kemunculan Rommy Sulastyo sebagai ayah Aldebaran di sinetron Ikatan Cinta mampu menarik perhatian publik.</p>
            <div class="article-detail--info">
                <div class="author">
                    Admin
                </div>
            </div>
        </div>

        <div class="share-baru-header">
            <a href="#">
                <img src="assets/images/share/fb.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/twitter.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/line.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/tele.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/wa.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/link.svg" alt="">
            </a>
        </div>

        <figure class="article-detail-figure">
            <img alt="image" src="assets/images/thumb9.jpg" class="card-headline-img"/>
            <figcaption>Nadiem Makarim (Instagram/Kemdikbud.RI)</figcaption>
        </figure>

    <a href="#!" rel="">
        <div class="banner-ads--big">
            <img src="assets/images/ads_baru/lead.svg" alt="" width="320px" height="100px">
        </div>
    </a>

        <div class="t0-b20">
            <div class="article-detail--body">
                <p><strong>indoparents.com</strong> - Diplomat senior Indonesia Dino Patti Djalal membeberkan tiga bukti yang menunjukkan bahwa seseorang bernama Fredy Kusnadi diduga terlibat dalam kasus penggelapan sertifikat tanah milik ibundanya.</p>
                <p>Dino menuding Fredy merupakan sindikat mafia tanah yang menggelapkan sertifikat tanah ibundanya tersebut.</p>
                <p>"Saya ingin memberikan tiga bukti mengenai keterlibatan Fredy dalam sindikat mafia tanah," ujar Dino dalam video yang diunggah di akun Instagram miliknya, Senin (15/2/2021) dini hari.</p>
                <p>Dino menuturkan, bukti pertama yang dimilikinya, yakni rekaman pengakuan dari seseorang bernama Sherly. Sherly, kata Dino, saat ini telah ditangkap polisi dan berstatus tersangka.</p>
                <div class="read-also-wrap">
                    <span class="read-also-wrap-title">Baca Juga:</span>
                    <a href="" class="read-also-wrap-item">Hurricane Ida updates: At least 1 death in Louisiana as New Orleans loses power</a>
                </div>
                <p>"Saya memberikan apresiasi dan terima kasih karena Sherly telah memberikan pengakuan yang sejujur-jujurnya mengenai peran Fredy dalam salah satu aksi penipuan terhadap rumah ibu saya," ucap Dino sebagaimana dilansir dari Antara.</p>
                
                <a href="#!" rel="">
                    <div class="banner-ads--big">
                        <img src="assets/images/ads_baru/mr1.svg" alt="" width="336px" height="280px">
                    </div>
                </a>

                <p>Bukti kedua yang disampaikan Dino, yakni bukti transfer uang. Uang tersebut diduga merupakan bagian dari hasil penggadaian sertifikat rumah milik ibunya di suatu koperasi.</p>
                <p>"Bukti kedua yang saya miliki dan sudah saya berikan ke polisi adalah bukti transfer yang diterima Fredy sebesar Rp 320 juta. Ini adalah sebagai bagian dari hasil penggadaian sertifikat rumah milik ibu saya ke suatu koperasi. Dari sana diuangkan sekitar Rp 4 (miliar) sampai Rp 5 miliar dan dibagi-bagi di antara mereka. Paling besar mungkin itu bosnya mendapat Rp 1,7 miliar. Yang lain antara Rp 1 miliar dan Rp 500 juta," kata Dino.</p>
                <div class="video-read-also">
                    <span class="read-also-wrap-title">Video Terkait:</span>
                    <div class="video-read-also-img-wrap">
                        <img alt="image" class="video-read-also-img" src="assets/images/thumb4.jpg"/>
                        <svg class="video-read-also-img--play" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-play-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z"/>
                        </svg>
                    </div>
                    <h3 class="live-report-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h3>
                </div>
                
                <p>"Jadi jelas nama Fredy ada di berbagai kasus rumah, sedikitnya tiga rumah, tapi mungkin lebih dari itu," sambung dia</p>
            </div>
            <div class="article-detail-pagination">
                <a href="" class="active">1</a>
                <a href="?page=detail">2</a>
                <a href="?page=detail">3</a>
                <a href="?page=detail">4</a>
                <a href="" class="show-all">Tampilkan Semua</a>
            </div>



            <div class="article-detail-tag">
                <span class="label card-headline-no-image-title">Tag</span>
                <a href="" class="tag-item">SIKM</a>
                <a href="" class="tag-item">Anies Baswedan</a>
                <a href="" class="tag-item">PSBB</a>
            </div>

            <div class="share-baru-bottom">
                <a href="#">
                    <img src="assets/images/share/fb.svg" alt="">
                </a>

                <a href="#">
                    <img src="assets/images/share/twitter.svg" alt="">
                </a>

                <a href="#">
                    <img src="assets/images/share/line.svg" alt="">
                </a>

                <a href="#">
                    <img src="assets/images/share/tele.svg" alt="">
                </a>

                <a href="#">
                    <img src="assets/images/share/wa.svg" alt="">
                </a>

                <a href="#">
                    <img src="assets/images/share/link.svg" alt="">
                </a>
            </div>

        </div>
    </article>

    <a href="#!" rel="">
        <div class="banner-ads--big">
            <img src="assets/images/ads_baru/mr2.svg" alt="" width="336px" height="280px">
        </div>
    </a>
    
    <div class="hot-video">
        <h3 class="hot-video--title">HOT VIDEO</h3>
        <div class="hot-video-list-scroll">
            <div class="hot-video-list">
                <div class="bg-card-video-item">
                <article class="hot-video-list--item">
                    <img alt="image" class="hot-video-list--item-img" src="assets/images/thumb7.jpg"/>
                    
                        <div class="bottom-left-play">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/play.svg" /></a>
                        </div>
                        <div class="bottom-left-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/1.svg" /></a>
                        </div>
                        <div class="bottom-left-text"> Daily Video </div>
                        <div class="top-right-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/2.svg" /></a>
                        </div>
                    
                    <div class="hot-video-list--item-info">
                        <h4 class="hot-video-list--item-title">
                            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                        </h4>
                        <!-- <span class="hot-video-list--item-category">Seleb</span> -->
                    </div>
                </article>
                </div>

                <div class="bg-card-video-item">
                <article class="hot-video-list--item">
                    <img alt="image" class="hot-video-list--item-img" src="assets/images/thumb7.jpg"/>
                    
                        <div class="bottom-left-play">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/play.svg" /></a>
                        </div>
                        <div class="bottom-left-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/3.svg" /></a>
                        </div>
                        <div class="bottom-left-text"> Daily Video </div>
                        <div class="top-right-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/4.svg" /></a>
                        </div>

                    <div class="hot-video-list--item-info">
                        <h4 class="hot-video-list--item-title-2">
                            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                        </h4>
                        <!-- <span class="hot-video-list--item-category">Seleb</span> -->
                    </div>
                </article>
                </div>

                <div class="bg-card-video-item">
                <article class="hot-video-list--item">
                    <img alt="image" class="hot-video-list--item-img" src="assets/images/thumb7.jpg"/>
                    
                        <div class="bottom-left-play">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/play.svg" /></a>
                        </div>
                        <div class="bottom-left-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/1.svg" /></a>
                        </div>
                        <div class="bottom-left-text"> Daily Video </div>
                        <div class="top-right-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/2.svg" /></a>
                        </div>
                    
                    <div class="hot-video-list--item-info">
                        <h4 class="hot-video-list--item-title">
                            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                        </h4>
                        <!-- <span class="hot-video-list--item-category">Seleb</span> -->
                    </div>
                </article>
                </div>

                <div class="bg-card-video-item">
                <article class="hot-video-list--item">
                    <img alt="image" class="hot-video-list--item-img" src="assets/images/thumb7.jpg"/>
                    
                        <div class="bottom-left-play">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/play.svg" /></a>
                        </div>
                        <div class="bottom-left-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/3.svg" /></a>
                        </div>
                        <div class="bottom-left-text"> Daily Video </div>
                        <div class="top-right-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/4.svg" /></a>
                        </div>
                    
                    <div class="hot-video-list--item-info">
                        <h4 class="hot-video-list--item-title-2">
                            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                        </h4>
                        <!-- <span class="hot-video-list--item-category">Seleb</span> -->
                    </div>
                </article>
                </div>

                <div class="bg-card-video-item">
                <article class="hot-video-list--item">
                    <img alt="image" class="hot-video-list--item-img" src="assets/images/thumb7.jpg"/>
                    
                        <div class="bottom-left-play">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/play.svg" /></a>
                        </div>
                        <div class="bottom-left-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/1.svg" /></a>
                        </div>
                        <div class="bottom-left-text"> Daily Video </div>
                        <div class="top-right-bg">
                            <a href="?page=detail"><img alt="image" src="assets/images/hot-video/2.svg" /></a>
                        </div>
                    
                    <div class="hot-video-list--item-info">
                        <h4 class="hot-video-list--item-title">
                            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                        </h4>
                        <!-- <span class="hot-video-list--item-category">Seleb</span> -->
                    </div>
                </article>
                </div>

            </div>
        </div>
    </div>

    <div class="mt-20">
        <h3 class="base-title pl-20 mb-15">Terpopuler</h3>
        <div class="card-slider">
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img alt="image" class="card-slider--item-img" src="assets/images/thumb4.jpg"/>
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img alt="image" class="card-slider--item-img" src="assets/images/thumb5.jpg"/>
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img alt="image" class="card-slider--item-img" src="assets/images/thumb7.jpg"/>
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Konsultasi</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img alt="image" class="card-slider--item-img" src="assets/images/thumb3.jpg"/>
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img alt="image" class="card-slider--item-img" src="assets/images/thumb7.jpg"/>
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    </div>
    
    <!-- start parenting -->
    <div class="mt-20">
        <h3 class="base-title pl-20 mb-15">Parenting</h3>
        <article class="card-headline">
            <img alt="image" class="card-headline-img" width="745" height="489" src="assets/images/thumb9.jpg"/>
            <div class="card-headline-info">
                <h4 class="card-headline-title">
                    <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                </h4>
            </div>
        </article>
        
		<div class="card-headline-small-wrap">
            <article class="card-headline-small">
                <img alt="image" class="card-headline-small-img" src="assets/images/thumb6.jpg"/>
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-headline-small">
                <img alt="image" class="card-headline-small-img" src="assets/images/thumb3.jpg"/>
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-headline-small">
                <img alt="image" class="card-headline-small-img" src="assets/images/thumb2.jpg"/>
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-headline-small">
                <img alt="image" class="card-headline-small-img" src="assets/images/thumb5.jpg"/>
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <!-- end parenting -->



    <!-- start terkini -->
    <div class="mt-20">
        <h3 class="base-title pl-20">Terkini</h3>
    <div>

    <div class="list">
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb2.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb3.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb4.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb5.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb6.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb7.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb8.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb9.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" width="350" height="230" src="https://images.unsplash.com/photo-1568715684971-9ac138754ab9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=400&q=80" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" width="350" height="230" src="https://images.unsplash.com/photo-1568715684971-9ac138754ab9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=400&q=80" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    
    </div>

    <div class="t10-b20 mb-20">
        <button class="main-card-loadmore" id="loadmore">Tampilkan lebih banyak</button>
    </div>

    <script>
    const loadmore = document.querySelector('#loadmore');
    let currentItems = 5;
    loadmore.addEventListener('click', (e) => {
        const elementList = [...document.querySelectorAll('.list .list-element')];
        for (let i = currentItems; i < currentItems + 5; i++) {
            if (elementList[i]) {
                elementList[i].style.display = 'block';
            }
        }
        currentItems += 5;

        // Load more button will be hidden after list fully loaded
        if (currentItems >= elementList.length) {
            event.target.style.display = 'none';
        }
    })
</script>