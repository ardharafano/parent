<div class="main-content mt-10 mb-20">
    <!-- <div id="div-ad-top" data-ad-type="msite_top" class="ads ads--top_home">
        <script type="text/javascript">
            googletag.cmd.push(function() { googletag.display('div-ad-top'); });
        </script>
    </div> -->
    <?php // include ("components/artist.php"); ?>
    <div class="kanal-wrap">
        <h3 class="base-title-desc">REDAKSI</h3>
    </div>
    <article class="article-detail mt-20">
        <div class="t0-b20">
            <div class="article-detail--body">
				<div class="list-article">
                    <ul>
                        <li><a href="?page=kanal">Pimpinan Redaksi</a></li>
                            <div class="list-nama">
                                <p>William Bradley Pitt</p>
                            </div>
                        <li><a href="?page=kanal">Redaktur Pelaksana</a></li>
                        	<div class="list-nama">
                                <p>Shiloh Jolie-Pitt</p>
                                <p>Hal Knox Hillhouse</p>
                                <p>Elizabeth Brown</p>
                            </div>
                        <li><a href="?page=kanal">Editor</a></li>
                        	<div class="list-nama">
                                <p>William Alvin Pitt</p>
                                <p>Hal Knox Hillhouse</p>
                                <p>Clara Mae Bell</p>
                            </div>
                    </ul>
                </div>
            </div>

        </div>
    
    </div>