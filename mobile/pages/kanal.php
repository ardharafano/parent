<div class="main-content mt-20 mb-20">
    <!-- <div id="div-ad-top" data-ad-type="msite_top" class="ads ads--top_home">
        <script type="text/javascript">
            googletag.cmd.push(function() { googletag.display('div-ad-top'); });
        </script>
    </div> -->

    <a href="#!" rel="">
        <div class="banner-ads--big">
            <img src="assets/images/ads_baru/lead.svg" alt="" width="320px" height="100px">
        </div>
    </a>

    <div class="kanal-wrap mt-15 mb-10">
        <h3 class="base-title">Parenting</h3>
        <div class="date">
            Kamis, 16 September 2021
        </div>
    </div>
    <div>
        <article class="card-headline">
            <img class="card-headline-img" alt="image" src="assets/images/thumb2.jpg" />
            <div class="card-headline-info">
                <h4 class="card-headline-title">
                    <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                </h4>
            </div>
        </article>
        <div class="card-headline-small-wrap">
            <article class="card-headline-small">
                <img class="card-headline-small-img" alt="image" src="assets/images/thumb1.jpg" />
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Parenting</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-headline-small">
                <img class="card-headline-small-img" alt="image" src="assets/images/thumb2.jpg" />
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-headline-small">
                <img class="card-headline-small-img" alt="image" src="assets/images/thumb3.jpg" />
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-headline-small">
                <img class="card-headline-small-img" alt="image" src="assets/images/thumb5.jpg" />
                <div class="card-headline-small-info">
                    <h4 class="card-headline-small-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <!-- <span class="card-headline-small-category">Seleb</span> -->
                    <div class="category-and-time-head">
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    </div>

    <div class="live-report-card m-20">
        <h2 class="live-report-card--heading">Video</h2>
        <div class="live-report-card-img-wrap">
            <img class="live-report-card-img" alt="image" src="assets/images/thumb4.jpg" />
            <svg class="live-report-card-img--play" xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                fill="currentColor" class="bi bi-play-circle" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path
                    d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z" />
            </svg>
        </div>
        <h3 class="live-report-card--title">
            <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
        </h3>
        <div class="live-report-card-wrap">
            <a href="" class="live-report-card-more">Video Lainnya</a>
        </div>
    </div>

    <a href="#!" rel="">
        <div class="banner-ads--big">
            <img src="assets/images/ads_baru/mr1.svg" alt="" width="336px" height="280px">
        </div>
    </a>

    <div class="mt-20">
        <h3 class="base-title pl-20 mb-10">Terpopuler</h3>
        <div class="card-slider">
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img class="card-slider--item-img" alt="image" src="assets/images/thumb6.jpg" />
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img class="card-slider--item-img" alt="image" src="assets/images/thumb7.jpg" />
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Konsultasi</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img class="card-slider--item-img" alt="image" src="assets/images/thumb8.jpg" />
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
            <article class="card-slider--item">
                <div class="card-slider--item-img-wrap">
                    <img class="card-slider--item-img" alt="image" src="assets/images/thumb2.jpg" />
                </div>
                <div class="card-slider--item-info">
                    <h4 class="card-slider--item-title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>
    </div>

    <!-- start terkini -->
    <div class="list">

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb1.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb6.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb9.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb7.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb6.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb5.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb4.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Konsultasi</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb3.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb2.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Health</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

        <div class="list-element">
            <article class="main-card">
                <div class="main-card-img-wrap">
                    <img alt="image" class="main-card-img" src="assets/images/thumb1.jpg" />
                </div>
                <div class="main-card--info">
                    <h4 class="main-card--title">
                        <a href="?page=detail">8 Fakta Megan Fox, Aktris yang Tampil Sangat Seksi di MTV VMA 2021</a>
                    </h4>
                    <div class="category-and-time">
                        <a href="?page=detail">Parenting</a>
                        <span>11:15 WIB</span>
                    </div>
                </div>
            </article>
        </div>

    </div>

    <div class="t10-b20">
        <button class="main-card-loadmore" id="loadmore">Tampilkan lebih banyak</button>
    </div>

    <script>
    const loadmore = document.querySelector('#loadmore');
    let currentItems = 5;
    loadmore.addEventListener('click', (e) => {
        const elementList = [...document.querySelectorAll('.list .list-element')];
        for (let i = currentItems; i < currentItems + 5; i++) {
            if (elementList[i]) {
                elementList[i].style.display = 'block';
            }
        }
        currentItems += 5;

        // Load more button will be hidden after list fully loaded
        if (currentItems >= elementList.length) {
            event.target.style.display = 'none';
        }
    })
    </script>