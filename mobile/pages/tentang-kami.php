<div class="main-content mt-10 mb-20">
    <!-- <div id="div-ad-top" data-ad-type="msite_top" class="ads ads--top_home">
        <script type="text/javascript">
            googletag.cmd.push(function() { googletag.display('div-ad-top'); });
        </script>
    </div> -->
    <?php // include ("components/artist.php"); ?>
    <div class="kanal-wrap">
        <h3 class="base-title-desc">TENTANG KAMI</h3>
    </div>
    <article class="article-detail">
        <div class="t0-b20">
            <div class="article-detail--body">

                <p><strong>Indoparents.com</strong> merupakan situs parenting, kesehatan, dan gaya hidup yang tercipta pada Oktober 2022. Kami hadir untuk memenuhi kebutuhan para orangtua muda dengan mobilitas tinggi. </p>

                <p>Dikemas dengan kompisisi multimedia serta bahasa yang lentur, Indoparents menjadi website berita kesehatan, gaya hidup, dan parenting yang ideal. Sekaligus menjadi solusi jitu dalam menjaga keharmonisan rumah tangga.</p>

                <p>Diangkat dari perspektif orangtua muda dan kaum millennials, kami mengemas konten melalui artikel, foto, infografis, dan video.</p>
                <p>Goals Indoparents sudah barang tentu mengedukasi dan membantu para orangtua muda menciptakan rumah tangga harmonis, penuh kasih sayang, sejahtera, dan sehat. </p>
                <p>Nikmati sensasi berjelajah di <strong>Indoparents.com</strong>.</p>

            </div>

        </div>

</div>