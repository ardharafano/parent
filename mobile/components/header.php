<!-- parallax -->
<!-- <div id="adds-top" class="add adds-top">
    <div class="wrap-adds">
        <div id='div-ad-parallax'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-ad-parallax'); });
            </script>
        </div>  
    </div>
    <div id="close-adds-top">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
        </svg>
    </div>
</div> -->
<!-- end parallax -->
<header class="wrap-header">
    <div class="main-header">

            <div class="header-menu--wrap noselect" id="open-menu">
                <svg id="menu-open" class="menu-open" xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                </svg>
                <svg id="menu-close" class="menu-close" style="display: none;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                </svg>
            </div>

        <div class="header-logo--wrap">
            <a href="?page=home">
                <img src="assets/images/logo.svg" width="155" height="24" alt="Logo-IndoParent.com" class="logo">
            </a>
        </div>

        <div class="menu-header">
            <div class="menu-search">
                <button class="button-search" aria-label="button">

                <svg id="open-search" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>

                </button>
            </div>
        </div>

    </div>

    <div class="wrap-search">
        <form action="search" method="GET">
            <input type="text" placeholder="Cari berita.." name="q" />
            <button type="submit">Cari</button>
        </form>
    </div>

    <div id="main-menu" style="display: none;">
        <!-- <div class="search-form">
            <input class="input-search" placeholder="Cari disini">
            <button class="button-search">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>
            </button>
        </div> -->
        <div>
            <ul class="main-menu">
                <li class="menu-item active">
                    <a href="?page=kanal">HOME</a>
                </li>
                <li class="menu-item">
                    <a href="?page=kanal">PARENTING</a>
                </li>
                <li class="menu-item">
                    <a href="?page=kanal">LIFESTYLE</a>
                </li>
                <li class="menu-item">
                    <a href="?page=kanal">HEALTH</a>
                </li>
                <li class="menu-item">
                    <a href="?page=kanal">VIDEO</a>
                </li>
                <li class="menu-item">
                    <a href="?page=kanal">KONSULTASI</a>
                </li>
            </ul>
        </div>
        
    <?php include ("components/footer.php"); ?>

</header>


<!-- start search navbar -->
<script>
    document.getElementById("open-search").onclick = function() {
        var e = document.getElementsByClassName("wrap-search")[0];
        if ("" === e.style.display || "none" === e.style.display) {
            var n = "\n            -webkit-animation: expand-open-search 0.1s;\n            display: block;\n        ";
            e.style = "".concat(n)
        } else {
            n = "\n            -webkit-animation: expand-close-search 0.1s;\n            display: none;\n        ";
            e.style = "".concat(n)
        }
    }
</script>
<!-- end search navbar -->